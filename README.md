# A8Parser
This SDK parses out details from a PAN/Aadhaar image taken by the mobile camera or an image chosen from the Gallery .

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

## Installation

Step 1. Add the maven repository to your build file
Add it in your root build.gradle at the end of repositories:
```gradle
	allprojects {
		repositories {
			...
    		maven {
                url 'https://maven.autonom8.com/artifactory/gradle-release-local'
                credentials {
                    username = "${artifactory_user}"
                    password = "${artifactory_password}"
                }
            }
		}
	}
```
  
Step 2. Add the dependency
```gradle
	dependencies {
		compile 'com.autonom8.parser:a8parser:1.0.+'
	}
```
  
That's it! The first time you request a project Maven checks out the code, builds it and serves the build artifacts.

Step 3. Sync the project 


## Using The Parser

Step 1: Declare A8Parser 
```java
	class MainActivity extends AppCompatActivity {
		//..
		A8Parser a8Parser;
		//..
```
Step 2: Initialize A8Parser Instance.
        Pass the License Key to the initialize() method.
```java    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        a8Parser = A8Parser.getInstances();
        a8Parser.initalize(context,appKey); //appKey is the license key
	    //..
```
### Parse Custom API [Optional]

```java
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //..
        setPanApi(String URL,String version);
        setAadharApi(String URL,String version)
        setDrivingLicenseApi(String drivingLicenseUrl,String version)
        setChequeApi(String chequeUrl,String version);
        setPassportApi(String passportUrl,String version);
        setVoterIdApi(String voterIdUrl,String version);
        //..
``` 
Step 3: Use callback method of pan/aadhar wherever needed

```Pan Card:```
```java
   	a8Parser.getPan(new ImageScannerListener<PanDetails>() {
                @Override
                public void onResult(PanDetails panDetails) {
			        //Result of Pan Card  
                }

            });
```
```Aadhar Card:```
```java
     String side = "front"; //or back
	 a8Parser.getAadhar(side,new ImageScannerListener<AadharDetails>() {
                @Override
                public void onResult(AadharDetails aadharDetails) {
			        //Result of Aadhar Card
                }
            });
```
```Driving License Details:```
```java
	 a8Parser.getDrivingLicenseDetails(new ImageScannerListener<DrivingLicenseDetails>() {
                @Override
                public void onResult(DrivingLicenseDetails drivingLicenseDetails) {
			        //Result of Driving License
                }
            });
```

```Cheque Details:```
```java
	 a8Parser.getChequeDetails(new ImageScannerListener<ChequeDetails>() {
                @Override
                public void onResult(ChequeDetails ChequeDetails) {
			        //Result of Cheque
                }
            });
```
```Passport Details:```
```java
     String side = "front"; // or back
	 a8Parser.getPassportDetails(side,new ImageScannerListener<PassportDetails>() {
                @Override
                public void onResult(PassportDetails passportDetails) {
			        //Result of Passport Details
                }
            });
```
```VoterId Details:```
```java
	 a8Parser.getPassportDetails(new ImageScannerListener<VoterIdDetails>() {
                @Override
                public void onResult(VoterIdDetails VoterIdDetails) {
			        //Result of VoterId Details
                }
            });
```

### Parse Result 

###### Pan Card

```java
    @Override
    public void onResult(PanDetails panDetails) {
        Log.d("Name:",panDetails.getName());
        Log.d("FathersName:",panDetails.getFathersName());
        Log.d("AccountNumber:",panDetails.getAccountNumber());
        Log.d("Date Of Birth:",panDetails.getDob());
    }
```

###### Aadhar Card
  
```java
    @Override
    public void onResult(AadharDetails aadharDetails) {
        Log.d("Name:",aadharDetails.getName());
        Log.d("Gender:",aadharDetails.getGender());
        Log.d("FathersName:",aadharDetails.getFathersName());
        Log.d("AccountNumber:",aadharDetails.getAadharNumber());
        Log.d("Date Of Birth:",aadharDetails.getDob());
    }
``` 

### Picture Quality [Optional]

```java
    @Override
    public void onCreate() {
        //...
        /** 
            Values can be LOW, HIGH, MEDIUM [default:HIGH]
        **/
        A8Parser.getInstance().setPictureQuality(PictureSize.MEDIUM)
        //...
    }
```