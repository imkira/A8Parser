package com.autonom8.parser.imagescanner;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.autonom8.parser.a8.pojo.ChequeDetails;
import com.autonom8.parser.a8.pojo.DrivingLicenseDetails;
import com.autonom8.parser.a8.pojo.PassportDetails;
import com.autonom8.parser.a8.pojo.VoterIdDetails;
import com.intsig.scanner.ScannerSDK;
import com.intsig.view.ImageEditView;
import com.intsig.view.ImageEditView.OnCornorChangeListener;

import com.autonom8.parser.R;
import com.autonom8.parser.a8.interfaces.ImageScannerListener;
import com.autonom8.parser.a8.pojo.AadharDetails;
import com.autonom8.parser.a8.pojo.PanDetails;
import com.autonom8.parser.a8.A8Parser;
import com.autonom8.parser.a8.A8Designer;
import com.autonom8.parser.imagescanner.ImageScannerLibs.utils.BmpUtils;
import com.autonom8.parser.imagescanner.ImageScannerLibs.utils.CaptureUtils;
import com.autonom8.parser.imagescanner.ImageScannerLibs.utils.DocumentUtil;
import com.autonom8.parser.imagescanner.ImageScannerLibs.views.CameraSurfaceView;
import com.autonom8.parser.imagescanner.ImageScannerLibs.views.CamPreview;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * This view is used to show image's crop and enhance process
 */
public class ImageScannerActivity extends Activity implements
        OnItemSelectedListener, OnClickListener,
        CameraSurfaceView.OnCameraStatusListener {


    private static final String TAG = ImageScannerActivity.class
            .getSimpleName();
    private static final int REQ_CODE_GALLERY_IMPORT = 0;
    /**
     * Normal crop color
     */
    private final static int mNormalColor = 0xff19BC9C;
    /**
     * Abnormal crop color
     */
    private final static int mErrorColor = 0xffff9500;
    private static final int TRIM_IMAGE_MAXSIDE = 1024;
    /**
     * your app key
     */
//    private static final String APPKEY = "035gSMBbaa2eSF4J1aM4Y7MX";// 2017
    public static boolean isClicked = false;
    ////A8Parser initialization
    public static ImageScannerListener<PanDetails> panScannerListener;
    public static ImageScannerListener<AadharDetails> aadhaarScannerListener;
    public static ImageScannerListener<DrivingLicenseDetails> drivingScannerListener;
    public static ImageScannerListener<ChequeDetails> chequeScannerListener;
    public static ImageScannerListener<PassportDetails> passportScannerListener;
//    public static ImageScannerListener<AccValidatorDetails> accValidatorScannerListener;
//    public static ImageScannerListener<SalarySlipDetails> salarySlipScannerListener;
    public static ImageScannerListener<VoterIdDetails> voterIdScannerListener;
    private static SimpleDateFormat sPdfTime = new SimpleDateFormat(
            "yyyy-MM-dd_HH-mm-ss");
    /**
     *
     */
    private final int REQUEST_CAPTURE_PIC = 100;
    private final int RC_GET_PICTURE = 301;
    public String imageString;
    public ImageScannerActivity context;
    public String FILENAME;
    RelativeLayout mTakePhotoLayout;
    CameraSurfaceView mCameraPreview;
    boolean boolClick = true;
    TextView bt_addButton, bt_add_from_camera;
    /**
     * Enter the enhance image layout
     */
    Integer selectSaveImgType = 0;
    /**
     * This view is used to show image enhance result
     */
    private ImageView mIVEnhance;
    /**
     * This view is used to show image crop result
     */
    private ImageEditView mIvEditView;
    /**
     * Crop layout
     */
    private View mTrimView;
    /**
     * Enhance layout
     */
    private View mEnhanceView;
    /**
     * The layout used to add image
     */
    private View take_photo_layout;
    private TextView mBtnNext;
    private String mRootPath;
    //problem
    private String mOriTrimImagePath;
    /**
     * used to show the enhance model set
     */
    private Spinner mSpinner;
    private Spinner typemSpinner;
    // 0701
    private ScannerSDK mScannerSDK;
    /**
     * / Compress image/source image
     */
    private float mScale = 1.0f;
    /**
     * Original enhance image
     */
    private Bitmap mOriginalEnhanceBitmap;
    /**
     * Enhancing image
     */
    private Bitmap mEnhanceBitmap;
    //#end
    /**
     * Current input image path
     */
    private String mCurrentInputImagePath;
    /**
     * Last detected border by engine
     */
    private int[] mLastDetectBorder;
    private int mEngineContext;
    private float angle = 0;
    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            if (msg.what != 0) {
                Toast.makeText(ImageScannerActivity.this,
                        "-->" + msg.what, Toast.LENGTH_LONG).show();
                boolClick = false;
            }

            super.handleMessage(msg);
        }

    };
    private int mEnhanceMode = ScannerSDK.ENHANCE_MODE_AUTO;
    private ProgressDialog mProgressDialog;
    private CamPreview mPreview;
    private RelativeLayout previewParent;
    private LinearLayout blackTop;
    private LinearLayout blackBottom;
    private ImageButton ibFlash;
    private ImageButton ibGrid;
    private ImageView ivGridLines;
    private LinearLayout llGallery;
    private TextView textCamera;
    private SeekBar sbZoom;
    private FrameLayout frameLayout;
    private CaptureUtils captureUtils;
    private boolean flashEnabled = false;
    private int activeCamera = 0;
    private OrientationEventListener orientationListener;
    private int screenOrientation = 90;
    Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            FileOutputStream outStream = null;
            try {
                System.gc();
                Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);

                FILENAME = "tmp" + System.currentTimeMillis() + ".jpg";

                outStream = openFileOutput(FILENAME, Context.MODE_PRIVATE);

                bmp.compress(Bitmap.CompressFormat.JPEG, 100, outStream);

                outStream.close();
                resetCam();
                String path = getFilesDir() + "/" + FILENAME;
                Log.e("A8Parser", "path" + path);

                mOriTrimImagePath = path;

                int orientation = 0;
                int fix = 1;

                if (activeCamera == 0) {
                    ExifInterface ei = new ExifInterface(mOriTrimImagePath);
                    orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    ei.setAttribute(ExifInterface.TAG_ORIENTATION, "90");
                    ei.saveAttributes();
                } else {
                    ExifInterface ei = new ExifInterface(mOriTrimImagePath);
                    orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    ei.setAttribute(ExifInterface.TAG_ORIENTATION, "0");
                    ei.saveAttributes();
                    orientation = ExifInterface.ORIENTATION_ROTATE_270;
                    fix = -1;
                }

                switch (orientation) {
                    case ExifInterface.ORIENTATION_UNDEFINED:
                        BmpUtils.rotateBitmap(path, normalizeRot(90 + screenOrientation));
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        BmpUtils.rotateBitmap(path, normalizeRot(90 + screenOrientation));
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        BmpUtils.rotateBitmap(path, normalizeRot(180 + screenOrientation));
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        BmpUtils.rotateBitmap(path, normalizeRot(270 + fix * screenOrientation));
                        break;
                    case ExifInterface.ORIENTATION_NORMAL:
                        BmpUtils.rotateBitmap(path, normalizeRot(screenOrientation));
                        break;
                }

                frameLayout.setVisibility(View.GONE);
                loadTrimImageFile(mOriTrimImagePath);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
            }
        }
    };
    private int THRESHOLD = 30;
    private ImageButton ibFlipCamera;

    /**
     * @param pathName
     * @param pathName
     * @return image size
     */
    private static int[] getImageSizeBound(String pathName) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inSampleSize = 1;
        BitmapFactory.decodeFile(pathName, options);
        int[] wh = null;
        if (options.mCancel || options.outWidth == -1
                || options.outHeight == -1) {
            Log.d(TAG, "getImageBound error " + pathName);
        } else {
            wh = new int[2];
            wh[0] = options.outWidth;
            wh[1] = options.outHeight;
        }
        return wh;
    }

    /**
     * @param size    * @param borders
     * @param size    : source image wide and high
     * @param borders : it's an int array of the 4 corner points
     * @return the new crop image area after the adjustment
     */
    private static float[] getScanBoundF(int[] size, int[] borders) {
        float[] bound = null;
        if (size != null) {
            if ((borders == null)) {
                Log.d(TAG, "did not found bound");
                bound = new float[]{0, 0, size[0], 0, size[0], size[1], 0,
                        size[1]};
            } else {
                bound = new float[8];
                for (int j = 0; j < bound.length; j++) {
                    bound[j] = borders[j];
                }
                for (int i = 0; i < 4; i++) {
                    if (bound[i * 2] < 0)// x
                        bound[i * 2] = 0;
                    if (bound[i * 2 + 1] < 0)// y
                        bound[i * 2 + 1] = 0;
                    if (bound[i * 2] > size[0])// x
                        bound[i * 2] = size[0];
                    if (bound[i * 2 + 1] > size[1])// y
                        bound[i * 2 + 1] = size[1];
                }
            }
        }
        return bound;
    }

    /**
     * Get rotation in degrees
     */
    private static int exifOrientationToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    public static Bitmap rotateImage(Bitmap sourceImage, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(sourceImage, 0, 0, sourceImage.getWidth(), sourceImage.getHeight(), matrix, true);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        ProxySelector.setDefault(new ProxySelector() {
            private final ProxySelector def = ProxySelector.getDefault();
            @Override
            public List<Proxy> select(final URI uri) {
                if ("http".equalsIgnoreCase(uri.getScheme()) || "https".equalsIgnoreCase(uri.getScheme())) {
                    if (uri.getHost().contains("bcrs.intsig.net")) {
                        Log.d(TAG, "Proxy bypassed host- " + uri.getHost());
                        return Arrays.asList(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("ace-eip.autonom8.com", 3428)));
                    }
                }
                return def.select(uri);
            }
            @Override
            public void connectFailed(final URI uri, final SocketAddress sa, final IOException ioe) {
                Log.e(TAG, "initProxyGateWay URI: " + uri +" SocketAddress:"+sa);
                Log.e(TAG, "initProxyGateWay connectFailed: ", ioe);
            }
        });
        //A8Parser
        A8Designer.activity = ImageScannerActivity.this;
        //#end
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.ac_scanner);
        ActivityCompat.requestPermissions(ImageScannerActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1);
        /*Log.e("pan2","object:"+panScannerListener);
        if(context.getIntent().getStringExtra("process").equals("pan")) {
            if (panScannerListener != null)
                panScannerListener.onStart();
        }
        else {
            if (aadhaarScannerListener != null)
                aadhaarScannerListener.onStart();
        }
*/
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
       //A8Parser
        mRootPath = Environment.getExternalStorageDirectory().getAbsolutePath();

        Log.e("A8Parser", ":" + getFilesDir());
        mRootPath += File.separator + "intsig" + File.separator
                + "demo_imagescanner";

        mOriTrimImagePath = mRootPath + File.separator + "oriTrim.jpg";

        File dir = new File(mRootPath);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        mScannerSDK = new ScannerSDK();

        new Thread(new Runnable() {

            @Override
            public void run() {
                int code = mScannerSDK.initSDK(ImageScannerActivity.this,
                        A8Parser.appKey);
                Log.e("APP KEY", ":" + A8Parser.appKey);
                mEngineContext = mScannerSDK.initThreadContext();
                mHandler.sendEmptyMessage(code);
                Log.d(TAG, "code=" + code);
            }
        }).start();
        initView();
        InitControls();
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    void InitControls() {

        previewParent = (RelativeLayout) findViewById(R.id.rlPreview);
//        blackTop = (LinearLayout) findViewById(R.id.llBlackTop);
        blackBottom = (LinearLayout) findViewById(R.id.llBlackBottom);
//        llGallery = (LinearLayout) findViewById(R.id.llGallery);

        ibFlash = (ImageButton) findViewById(R.id.ibFlash);
        ibGrid = (ImageButton) findViewById(R.id.ibGrid);
        ivGridLines = (ImageView) findViewById(R.id.ivGridLines);
        ibFlipCamera = (ImageButton) findViewById(R.id.ibFlipCamera);
        sbZoom = (SeekBar) findViewById(R.id.sbZoom);


        if (Camera.getNumberOfCameras() > 1) {
            ibFlipCamera.setVisibility(View.VISIBLE);
        } else {
            ibFlipCamera.setVisibility(View.GONE);
        }
        orientationListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_UI) {
            public void onOrientationChanged(int orientation) {
                if (isOrientation(orientation, 0))
                    screenOrientation = 0;
                else if (isOrientation(orientation, 90))
                    screenOrientation = 90;
                else if (isOrientation(orientation, 180))
                    screenOrientation = 180;
                else if (isOrientation(orientation, 270))
                    screenOrientation = 270;


            }
        };
    }

    protected boolean isOrientation(int orientation, int degree) {
        return (degree - THRESHOLD <= orientation && orientation <= degree + THRESHOLD);
    }

    private void setupCamera(final int camera) {
        // Set the second argument by your choice.
        // Usually, 0 for back-facing camera, 1 for front-facing camera.
        // If the OS is pre-gingerbreak, this does not have any effect.
        try {
            mPreview = new CamPreview(this, camera, CamPreview.LayoutMode.NoBlank);// .FitToParent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, R.string.cannot_connect_to_camera, Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        RelativeLayout.LayoutParams previewLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        // Un-comment below lines to specify the size.

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        int width = outMetrics.widthPixels;
        int height = outMetrics.heightPixels;

        previewLayoutParams.height = width;
        previewLayoutParams.width = width;

        // Un-comment below line to specify the position.
        mPreview.setCenterPosition(width / 2, height / 2);

        previewParent.addView(mPreview, 0, previewLayoutParams);

        // there is changes in calculations
        // camera preview image centered now to have actual image at center of
        // view
//        int delta = height - width;
//        int btHeight = 0;// blackTop.getHeight();
//        int fix = delta - btHeight;
//        int fix2 = 0;// fix / 4;
//
//        FrameLayout.LayoutParams blackBottomParams = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, fix / 2 + fix2);
//        blackBottomParams.gravity = Gravity.BOTTOM;
//        blackBottom.setLayoutParams(blackBottomParams);

//        FrameLayout.LayoutParams blackTopParams = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, fix / 2 - fix2);
//        blackTopParams.gravity = Gravity.TOP;
//        blackTop.setLayoutParams(blackTopParams);

        captureUtils = new CaptureUtils(mPreview.getCamera());

        if (captureUtils.isCameraFlashAvailable()) {
            captureUtils.toggleFlash(flashEnabled);
            ibFlash.setVisibility(View.VISIBLE);
            ibFlash.setImageResource(flashEnabled ? R.drawable.flash : R.drawable.flash_off);
        } else {
            ibFlash.setVisibility(View.GONE);
        }
        mPreview.setOnZoomCallback(new CamPreview.ZoomCallback() {
            @Override
            public void onZoomChanged(int progress) {
                sbZoom.setProgress(progress);
            }
        });
        sbZoom.setVisibility(captureUtils.hasAutofocus() ? View.VISIBLE : View.GONE);

        sbZoom.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (mPreview != null) {
                    mPreview.onProgressChanged(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                if (mPreview != null && seekBar != null) {
                    mPreview.onStopTrackingTouch(seekBar.getProgress());
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        isClicked = false;
//        checkLocationEnabled();
        int currentapiVersion = Build.VERSION.SDK_INT;


        orientationListener.enable();

        setupCamera(activeCamera);

        if (sbZoom.getVisibility() == View.VISIBLE) {
            sbZoom.setProgress(0);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();
        orientationListener.disable();
    }

    private void releaseCamera() {
        if (mPreview != null) {
            mPreview.stop();
            previewParent.removeView(mPreview); // This is necessary.
            mPreview = null;
        }
    }

    @SuppressLint("NewApi")
    public void flipClick(View view) {
        if (Build.VERSION.SDK_INT < 9)
            return;

        if (Camera.getNumberOfCameras() > 1) {

            activeCamera = activeCamera == 0 ? 1 : 0;
            releaseCamera();
            setupCamera(activeCamera);
        }
    }

    public void flashClick(View view) {
        if (!captureUtils.isCameraFlashAvailable())
            return;
        flashEnabled = !flashEnabled;
        captureUtils.toggleFlash(flashEnabled);
        ibFlash.setImageResource(flashEnabled ? R.drawable.flash : R.drawable.flash_off);
    }

    public void captureClick(View view) {
        try {
            if (!isClicked) {
                captureUtils.takeShot(jpegCallback);
                isClicked = true;
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    protected void resetCam() {
        Camera camera = mPreview.getCamera();
        camera.startPreview();
    }

    protected int normalizeRot(int rot) {
        if (rot < 0)
            rot += 360;
        if (rot > 360)
            rot -= 360;
        return rot;
    }

    public void gridClick(View v) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, REQ_CODE_GALLERY_IMPORT);
    }

    private void startCapture() {
        // if (mOriTrimImagePath == null) {
        // mTakePicPath = DIR_ICR + "card.jpg";
        // }
        File dstFile = new File(mOriTrimImagePath);
        Uri uri = Uri.fromFile(dstFile);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, REQUEST_CAPTURE_PIC);

    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
//        if (viewId == R.id.bt_add) {
////			if (!boolClick) {
////				Toast.makeText(ImageScannerActivity.this, "授权失败" + "-->-3",
////						Toast.LENGTH_LONG).show();
////				return;
////			}
//
//            // click 'add' button,and go to gallery
//            Log.d(TAG, "go2Gallery");
//            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//            intent.setType("image/*");
//            startActivityForResult(intent, REQ_CODE_GALLERY_IMPORT);
//        } else
        if (viewId == R.id.take_photo_id) {

            // mCameraPreview.takePicture();
        } else if (viewId == R.id.close_photo_id) {
//            mAddImageView.setVisibility(View.VISIBLE);
            take_photo_layout.setVisibility(View.GONE);
        }
//        else if (viewId == R.id.bt_add_from_camera) {
//            // click 'add' button,and go to gallery
//            // if(!boolClick){
//            // Toast.makeText(ImageScannerActivity.this,
//            // "授权失败" + "-->-3" , Toast.LENGTH_LONG).show();
//            // return;
//            // }
//            // Log.d(TAG, "take picture");
//            // mAddImageView.setVisibility(View.GONE);
//            // take_photo_layout.setVisibility(View.VISIBLE);
//            //
//            // mCameraPreview = (CameraSurfaceView)
//            // findViewById(R.id.cameraPreview);
//            // mCameraPreview.setListener(this);
//            // mCameraPreview.setOnTouchListener(mCameraPreview.onTouchListenerSurfaceListener);
//            // FocusView focusView = (FocusView) findViewById(R.id.view_focus);
//            startCapture();// 调用系统相机拍照
//        }
        else if (viewId == R.id.bt_back_add) {

            enterAddImageLayout();

//			 Bitmap bitmap=BitmapFactory.decodeFile(mCurrentInputImagePath);
//			 RotateBitmap bitmapRotate=new RotateBitmap(bitmap,90);
//
//			 mIvEditView.rotate(bitmapRotate,true);

        } else if (viewId == R.id.bt_enhance) {
            // click 'next' button, and start trim
//            startTtrim();
            if (mIvEditView.isCanTrim(mEngineContext)) {
                startTtrim();
            } else {
                Toast.makeText(ImageScannerActivity.this,
                        R.string.bound_trim_error, Toast.LENGTH_SHORT).show();
            }

        } else if (viewId == R.id.bt_back_trim) {
            enterTrimLayout();
//            enterAddImageLayout();
        } else if (viewId == R.id.bt_save) {
            try {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                mEnhanceBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                imageString = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                Log.e("ScanActivity", "on save image");

                Intent intent = new Intent();
                intent.putExtra("BitmapImage", byteArray);
                context.setResult(RESULT_OK, intent);

                //A8Parser
                String url = context.getIntent().getStringExtra("url");
                String side = context.getIntent().getStringExtra("side");
                String version = context.getIntent().getStringExtra("version");
                if (side != null)
                    Log.e("aadharSide", ":" + side);

                HttpCall httpCall = new HttpCall();
                httpCall.execute("6d3e47cb332f9a1e679e4b3712ef9e59", url, version, side);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (viewId == R.id.bt_rotate) {
            angle += 90;
            mEnhanceBitmap = rotateImage(mEnhanceBitmap, angle);
            mIVEnhance.setImageBitmap(mEnhanceBitmap);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        Log.d(TAG, "position=" + position);
        if (mOriginalEnhanceBitmap != null) {
            EnhanceTask enhanceTask = new EnhanceTask(getEnhanceMode(position));
            enhanceTask.execute();
        } else {
            Log.d(TAG, "mOriginalEnhanceBitmap=" + mOriginalEnhanceBitmap);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQ_CODE_GALLERY_IMPORT) {

                // hide the previous result
                mIvEditView.setRegionVisibility(false);
                progressExportImage(data);
            }
            if (requestCode == RC_GET_PICTURE) {// 拍照回来的
                loadTrimImageFile(mOriTrimImagePath);

            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mScannerSDK.destroyContext(mEngineContext);
        if (mOriginalEnhanceBitmap != null
                && !mOriginalEnhanceBitmap.isRecycled()) {
            mOriginalEnhanceBitmap.recycle();
        }
        if (mEnhanceBitmap != null && !mEnhanceBitmap.isRecycled()) {
            mEnhanceBitmap.recycle();
        }
    }

    private void initView() {
//        mAddImageView = findViewById(R.id.rl_add_image);
        frameLayout = (FrameLayout) findViewById(R.id.layout);
        take_photo_layout = findViewById(R.id.take_photo_layout);

        mTrimView = findViewById(R.id.rl_trim);
        mEnhanceView = findViewById(R.id.ll_enhance);
        mIvEditView = (ImageEditView) findViewById(R.id.iv_trim);
        mIvEditView.setDrapPoint(R.drawable.dragpoint);
        mIvEditView.setRegionVisibility(false);
        mIvEditView.setOnCornorChangeListener(new MyCornorChangeListener());
        mIvEditView.setOffset(getResources().getDimension(
                R.dimen.highlight_point_diameter));

//        bt_addButton = (TextView) findViewById(R.id.bt_add);
//        bt_addButton.setOnClickListener(this);
//        bt_add_from_camera = (TextView) findViewById(R.id.bt_add_from_camera);
//
//        bt_add_from_camera.setOnClickListener(this);
        findViewById(R.id.bt_back_add).setOnClickListener(this);
        findViewById(R.id.take_photo_id).setOnClickListener(this);
        findViewById(R.id.close_photo_id).setOnClickListener(this);

        mBtnNext = (TextView) findViewById(R.id.bt_enhance);
        mBtnNext.setOnClickListener(this);

        findViewById(R.id.bt_back_trim).setOnClickListener(this);
        findViewById(R.id.bt_save).setOnClickListener(this);
        findViewById(R.id.bt_rotate).setOnClickListener(this);

        mIVEnhance = (ImageView) findViewById(R.id.iv_enhance);
    }

    /**
     * Enter the add image layout
     */
    public void enterAddImageLayout() {
//        mAddImageView.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.VISIBLE);
        mEnhanceView.setVisibility(View.GONE);
        mTrimView.setVisibility(View.GONE);
    }

    /**
     * Enter the crop image layout
     */
    public void enterTrimLayout() {
//        mAddImageView.setVisibility(View.GONE);
        mEnhanceView.setVisibility(View.GONE);
        mTrimView.setVisibility(View.VISIBLE);
    }

    public void enterEnhanceLayout() {
        if (mSpinner == null) {
            mSpinner = (Spinner) findViewById(R.id.sp_enhance_mode);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter
                    .createFromResource(ImageScannerActivity.this,
                            R.array.arrays_enhance,
                            R.layout.spinner_checked_text);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinner.setAdapter(adapter);
            mSpinner.setOnItemSelectedListener(ImageScannerActivity.this);
        }

//        if (typemSpinner == null) {
//            typemSpinner = (Spinner) findViewById(R.id.sp_img_type_mode);
//            ArrayAdapter<CharSequence> adapter = ArrayAdapter
//                    .createFromResource(ImageScannerActivity.this,
//                            R.array.arrays_img_type,
//                            R.layout.spinner_checked_text);
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            typemSpinner.setAdapter(adapter);
//            typemSpinner
//                    .setOnItemSelectedListener(new OnItemSelectedListener() {
//
//                        @Override
//                        public void onItemSelected(AdapterView<?> arg0,
//                                                   View arg1, int position, long arg3) {
//                            // TODO Auto-generated method stub
//                            selectSaveImgType = position;
//                        }
//
//                        @Override
//                        public void onNothingSelected(AdapterView<?> arg0) {
//                            // TODO Auto-generated method stub
//
//                        }
//                    });
//        }

        mIVEnhance.setImageBitmap(mOriginalEnhanceBitmap);
//        mAddImageView.setVisibility(View.GONE);
        mEnhanceView.setVisibility(View.VISIBLE);
        mTrimView.setVisibility(View.GONE);
    }

    @Override
    public void onCameraStopped(String data) {
        take_photo_layout.setVisibility(View.GONE);
        // mCameraPreview.closeCamera();
        loadTrimImageFile(data);
    }

    /**
     * Start to crop the image
     */
    private void startTtrim() {
        if (TextUtils.isEmpty(mOriTrimImagePath)) {
            Log.e("Image", "empty");
            return;
        }
        if (!TextUtils.isEmpty(mCurrentInputImagePath)) {
            Log.e("Trim Task", "working");
            TrimTask trimTask = new TrimTask(mCurrentInputImagePath);
            trimTask.execute();
        }
    }

    /**
     * get enhance mode
     *
     * @param which <p>
     *              get enhance mode
     *              <p>
     *              <pre>
     *                                                                  0: Auto
     *                                                                  1：No enhance
     *                                                                  2：Enhance
     *                                                                  3：Enhance and Magic
     *                                                                  4：Gray
     *                                                                  5：Black-and-White
     *                                                     </pre>
     */
    public int getEnhanceMode(int which) {
        int mode = ScannerSDK.ENHANCE_MODE_AUTO;
        switch (which) {
            case 0:

                // Auto
                mode = ScannerSDK.ENHANCE_MODE_AUTO;
                break;
            case 1:

                // No Enhance
                mode = ScannerSDK.ENHANCE_MODE_NO_ENHANCE;
                break;
            case 2:
                // Enhance
                mode = ScannerSDK.ENHANCE_MODE_LINEAR;
                break;
            case 3:

                // Enhance and Magic
                mode = ScannerSDK.ENHANCE_MODE_MAGIC;
                break;
            case 4:
                // Gray
                mode = ScannerSDK.ENHANCE_MODE_GRAY;
                break;
            case 5:
                // Black-and-White
                mode = ScannerSDK.ENHANCE_MODE_BLACK_WHITE;
                break;
            default:
                mode = ScannerSDK.ENHANCE_MODE_AUTO;
        }
        return mode;
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(ImageScannerActivity.this);
            mProgressDialog.setProgress(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setMessage(getResources().getString(
                    R.string.a_msg_working));
        }
        mProgressDialog.show();
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    /**
     * @param src Save the result image--cropped and enhanced
     */
    public String saveBitmap2File(Bitmap src) {

        String imgTypeString = ".jpg";
        Bitmap.CompressFormat format = Bitmap.CompressFormat.JPEG;
        switch (selectSaveImgType) {
            case 0:
                imgTypeString = ".jpg";
                format = Bitmap.CompressFormat.JPEG;
                break;
            case 1:
                imgTypeString = ".png";
                format = Bitmap.CompressFormat.PNG;
                break;
            case 2:
                imgTypeString = ".jpeg";
                format = Bitmap.CompressFormat.JPEG;

                break;

            default:
                break;
        }

        String outPutFilePath = mRootPath + File.separator
                + sPdfTime.format(new Date()) + imgTypeString;
        FileOutputStream outPutStream = null;
        try {
            outPutStream = new FileOutputStream(outPutFilePath);
            src.compress(format, 100, outPutStream);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "FileNotFoundException", e);
        } finally {
            if (outPutStream != null) {
                try {
                    outPutStream.close();
                } catch (IOException e) {
                    Log.e(TAG, "IOException", e);
                }
            }
        }
        Log.d(TAG, "saveBitmap2File, outPutFilePath=" + outPutFilePath);
        return outPutFilePath;
    }

    /**
     * @param path
     * @return true image is valid
     * <p>
     * Whether the image is valid or not, only support .png and .jpg
     */
    public boolean isValidImage(String path) {
        return !TextUtils.isEmpty(path)
                && (path.endsWith("png") || path.endsWith("jpg") ||
                path.endsWith("jpeg")
                || path.endsWith("PNG") || path.endsWith("JPG") || path.endsWith("JPEG"));
    }

    /**
     * @param data Process the input image
     */
    private void progressExportImage(Intent data) {
        frameLayout.setVisibility(View.GONE);
        if (data != null) {
            Uri u = data.getData();
            Log.d(TAG, "data.getData()=" + u);
            if (u != null) {
                String path = DocumentUtil.getInstance().getPath(this, u);
                if (isValidImage(path)) {
                    loadTrimImageFile(path);
                } else {
                    Toast.makeText(this, R.string.a_msg_illegal,
                            Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            Log.d(TAG, "data==null");
        }
    }

    /**
     * @param imageFilePath Crop the image
     */
    private void loadTrimImageFile(final String imageFilePath) {
        //A8
        Log.e("isflash", "value:" + flashEnabled);
        if (flashEnabled) {
            flashEnabled = false;
            captureUtils.toggleFlash(flashEnabled);
            ibFlash.setImageResource(flashEnabled ? R.drawable.flash : R.drawable.flash_off);
        }
        if (TextUtils.isEmpty(imageFilePath)) {
            Log.d(TAG, "imageFilePath is empty");
            return;
        }
        File file = new File(imageFilePath);
        if (!file.exists()) {
            Log.d(TAG, "imageFilePath is not exist");
            return;
        }

        Log.d(TAG, "loadTrimImageFile, imageFilePath=" + imageFilePath);
        enterTrimLayout();
        mIvEditView.post(new Runnable() {

            @Override
            public void run() {
                // to prevent out of memory error when the image is too large,
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imageFilePath, options);
                int bitmapWidth = options.outWidth;
                int bitmapHeight = options.outHeight;
                Log.d(TAG, "bitmapWidth=" + bitmapWidth + " bitmapHeight="
                        + bitmapHeight);
                if (bitmapWidth > 0 && bitmapHeight > 0) {
                    int viewWidth = mIvEditView.getWidth();
                    int viewHeight = mIvEditView.getHeight();
                    if (viewWidth > 0 && viewHeight > 0) {
                        float scaleX = 1.0f * viewWidth / bitmapWidth;
                        float scaleY = 1.0f * viewHeight / bitmapHeight;
                        float scale = scaleX > scaleY ? scaleY : scaleX;
                        int inSampleSize = (int) (1 / scale);
                        if (inSampleSize == 0) {
                            inSampleSize = 1;
                        }
                        options.inSampleSize = inSampleSize;
                        options.inJustDecodeBounds = false;
                        Bitmap testBitmap = BitmapFactory.decodeFile(
                                imageFilePath, options);
                        mScale = 1.0f * testBitmap.getWidth() / bitmapWidth;
                        int[] imgBound = new int[]{bitmapWidth, bitmapHeight};
                        // Set the source image wide and high
                        mIvEditView.setRawImageBounds(imgBound);
                        // Load the image
                        mIvEditView.loadDrawBitmap(testBitmap);
                        mCurrentInputImagePath = imageFilePath;
                        DetectBorderTask detectTask = new DetectBorderTask(
                                imageFilePath);
                        detectTask.execute();
                    }
                } else {
                    Log.d(TAG, "bitmapWidth=" + bitmapWidth + " bitmapHeight="
                            + bitmapHeight);
                }
            }
        });
    }

    /**
     * detect image border
     */
    class DetectBorderTask extends AsyncTask<Void, Void, Boolean> {
        private long mStartTime;
        /**
         * it's a absolute path of the source image
         */
        private String mPath;
        private float[] mOrginBounds = null;

        public DetectBorderTask(String path) {
            mPath = path;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            boolean succed = false;
            long tempTime = 0;
            mStartTime = System.currentTimeMillis();
            int threadContext = mScannerSDK.initThreadContext();
            System.out
                    .println("DetectBorderTask, initThreadContext, cost time:"
                            + (System.currentTimeMillis() - mStartTime));
            tempTime = System.currentTimeMillis();
            int imageStruct = mScannerSDK.decodeImageS(mPath);
            System.out.println("DetectBorderTask, decodeImageS, cost time:"
                    + (System.currentTimeMillis() - tempTime));

            mLastDetectBorder = null;
            int[] imgBound = getImageSizeBound(mPath);
            if (imageStruct != 0) {
                // Detect the border of the image
                tempTime = System.currentTimeMillis();
                mLastDetectBorder = mScannerSDK.detectBorder(threadContext,
                        imageStruct);
                System.out.println("DetectBorderTask, detectBorder, cost time:"
                        + (System.currentTimeMillis() - tempTime));
                Log.d(TAG,
                        "detectAndTrimImageBorder, borders="
                                + Arrays.toString(mLastDetectBorder));
                tempTime = System.currentTimeMillis();
                mOrginBounds = getScanBoundF(imgBound, mLastDetectBorder);

                System.out.println("DetectBorderTask, fix border, cost time:"
                        + (System.currentTimeMillis() - tempTime));
                tempTime = System.currentTimeMillis();
                mScannerSDK.releaseImage(imageStruct);
                System.out.println("DetectBorderTask, releaseImage, cost time:"
                        + (System.currentTimeMillis() - tempTime));
                succed = true;
            } else {
                mOrginBounds = getScanBoundF(imgBound, null);
            }
            mScannerSDK.destroyContext(threadContext);
            System.out.println("DetectBorderTask, cost time:"
                    + (System.currentTimeMillis() - mStartTime));
            return succed;
        }

        protected void onPostExecute(Boolean result) {
            dismissProgressDialog();
            Log.d(TAG, "result=" + result);
            if (result) {
                // remark the cropped area
                if (mScale < 0.001 && mScale > -0.001) {
                    mScale = 1.0f;
                }
                if (mOrginBounds != null) {
                    // load the source image crop area
                    mIvEditView.setRegion(mOrginBounds, mScale);
                    // set the crop are to be shown
                    mIvEditView.setRegionVisibility(true);
                    // mIvEditView.showDrawPoints(false);
                    // mIvEditView.enableMovePoints(false);
                }
                mBtnNext.setVisibility(View.VISIBLE);
                //A8
                Log.e("Flash", "Value:" + flashEnabled);
                if (flashEnabled) {
                    Log.e("Flash2", "Value:" + flashEnabled);
                    flashEnabled = false;
                    captureUtils.toggleFlash(flashEnabled);
                    ibFlash.setImageResource(flashEnabled ? R.drawable.flash : R.drawable.flash_off);
                }

            } else {
                Log.d(TAG, "result=" + result);
            }
        }
    }

    /**
     * deal with the cropping asynchronous
     */
    class TrimTask extends AsyncTask<Void, Void, Boolean> {
        private long mStartTime;
        /**
         * The absolute path of the image going to be cropped
         */
        private String mPath;

        public TrimTask(String path) {
            mPath = path;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Log.d(TAG, "TrimTask, doInBackground");
            long tempTime = 0;
            boolean succeed = false;
            mStartTime = System.currentTimeMillis();
            int threadContext = mScannerSDK.initThreadContext();
            System.out.println("TrimTask, initThreadContext, cost time:"
                    + (System.currentTimeMillis() - mStartTime));

            tempTime = System.currentTimeMillis();
            int imageStruct = mScannerSDK.decodeImageS(mPath);
            Log.e("mPath", "value:" + mPath);
            System.out.println("TrimTask, decodeImageS, cost time:"
                    + (System.currentTimeMillis() - tempTime));

            if (imageStruct != 0) {
                // Detect the border and crop it
                tempTime = System.currentTimeMillis();
                // Get the edge of the image on the control
                int[] bound = mIvEditView.getRegion(false);
                Log.d(TAG, "bound=" + Arrays.toString(bound));
                mScannerSDK.trimImage(threadContext, imageStruct, bound,
                        TRIM_IMAGE_MAXSIDE);
                System.out.println("TrimTask, trimImage, cost time:"
                        + (System.currentTimeMillis() - tempTime));

                tempTime = System.currentTimeMillis();
                mScannerSDK.saveImage(imageStruct, mOriTrimImagePath, 80);
                System.out.println("TrimTask, saveImage, cost time:"
                        + (System.currentTimeMillis() - tempTime));

                tempTime = System.currentTimeMillis();
                mScannerSDK.releaseImage(imageStruct);
                System.out.println("TrimTask, releaseImage, cost time:"
                        + (System.currentTimeMillis() - tempTime));

                File file = new File(mOriTrimImagePath);
                if (file.exists()) {
                    tempTime = System.currentTimeMillis();
                    Bitmap trimBitmap = null;
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(mOriTrimImagePath, options);
                    int bitmapWidth = options.outWidth;
                    int bitmapHeight = options.outHeight;
                    Log.d(TAG, "bitmapWidth=" + bitmapWidth + " bitmapHeight="
                            + bitmapHeight);
                    if (bitmapWidth > 0 && bitmapHeight > 0) {
                        int viewWidth = mIvEditView.getWidth();
                        int viewHeight = mIvEditView.getHeight();
                        float scaleX = 1.0f * viewWidth / bitmapWidth;
                        float scaleY = 1.0f * viewHeight / bitmapHeight;
                        float scale = scaleX > scaleY ? scaleY : scaleX;
                        int inSampleSize = (int) (1 / scale);
                        if (inSampleSize == 0) {
                            inSampleSize = 1;
                        }
                        options.inSampleSize = inSampleSize;
                        options.inJustDecodeBounds = false;
                        trimBitmap = BitmapFactory.decodeFile(
                                mOriTrimImagePath, options);
                    }
                    if (mOriginalEnhanceBitmap != null
                            && !mOriginalEnhanceBitmap.isRecycled()) {
                        mOriginalEnhanceBitmap.recycle();
                    }
                    mOriginalEnhanceBitmap = trimBitmap;
                    try {
//                        file.delete();
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                    System.out
                            .println("TrimTask, BitmapFactory.decodeFile, cost time:"
                                    + (System.currentTimeMillis() - tempTime));
                } else {
                    Log.d(TAG, "file is not exist");
                }
                succeed = true;
            }
            mScannerSDK.destroyContext(threadContext);
            System.out.println("TrimTask, cost time:"
                    + (System.currentTimeMillis() - mStartTime));
            return succeed;
        }

        protected void onPostExecute(Boolean result) {
            dismissProgressDialog();
            Log.d(TAG, "result=" + result);
            if (result) {
                enterEnhanceLayout();
                mEnhanceBitmap = mOriginalEnhanceBitmap.copy(
                        mOriginalEnhanceBitmap.getConfig(), true);
            } else {
                Log.d(TAG, "result=" + result);
            }
        }
    }

    /**
     * Enhance the image asynchronous
     */
    class EnhanceTask extends AsyncTask<Void, Void, Void> {

        public EnhanceTask(int mode) {
            mEnhanceMode = mode;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Void doInBackground(Void... params) {
            int threadContext = mScannerSDK.initThreadContext();
            // Recycle the previous enhanced image
            if (mEnhanceBitmap != null && !mEnhanceBitmap.isRecycled()) {
                mEnhanceBitmap.recycle();
            }
            // Copy a piece
            mEnhanceBitmap = mOriginalEnhanceBitmap.copy(
                    mOriginalEnhanceBitmap.getConfig(), true);
            Log.d(TAG, "mEnhanceBitmap");
            mScannerSDK.enhanceImage(threadContext, mEnhanceBitmap,
                    mEnhanceMode);
            Log.d(TAG, "enhanceImage");
            mScannerSDK.destroyContext(threadContext);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            dismissProgressDialog();
            mIVEnhance.setImageBitmap(mEnhanceBitmap);
            Log.d(TAG, "finish, EnhanceTask");
        }
    }

    /**
     * @author zhongze_wu listen the event of crop area adjustment
     */
    private class MyCornorChangeListener implements OnCornorChangeListener {
        /**
         * Start to move the frame or the point
         */
        @Override
        public void onPreMove() {
        }

        /**
         * Moving finished
         */
        @Override
        public void onPostMove() {
        }

        @Override
        public void onCornorChanged() {
            if (mIvEditView != null) {
                if (mIvEditView.isCanTrim(mEngineContext)) {
                    mIvEditView.setLinePaintColor(mNormalColor);
                } else {
                    mIvEditView.setLinePaintColor(mErrorColor);
                }
                mIvEditView.invalidate();
            }
        }
    }

    //A8Parser http render
    private class HttpCall extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            String current = "";
            try {
                URL url;
                String x_api = params[0];

                HttpURLConnection urlConnection = null;
                try {
                    url = new URL(params[1]);
                    String version = params[2];
                    String side = params[3];
                    urlConnection = (HttpURLConnection) url
                            .openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("x-api-key", x_api);
                    if (!version.equals(""))
                        urlConnection.setRequestProperty("version", version);
                    urlConnection.setRequestProperty("Accept", "application/json");

                    JSONObject data = new JSONObject();
                    if(side !=null){
                        data.put("side",side);
                    }
                    data.put("file",imageString.trim());

                    Log.e("DataString","data:"+data.toString());

                    OutputStream os = urlConnection.getOutputStream();
                    os.write(data.toString().getBytes("UTF-8"));
                    os.close();

                    int responseCode = urlConnection.getResponseCode();
                    BufferedReader in = null;

                    if (responseCode == HttpsURLConnection.HTTP_OK) {
                        Log.e("HTTPCall","ResponseCode:"+responseCode);
                        in = new BufferedReader(
                                new InputStreamReader(
                                        urlConnection.getInputStream()));
                    } else {
                        Log.e("HTTPCall","ErrorResponseCode:"+responseCode);
                        in = new BufferedReader(
                                new InputStreamReader(
                                        urlConnection.getErrorStream()));
                    }
                    StringBuffer sb = new StringBuffer("");
                    String line = "";
                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    in.close();
                    return sb.toString();
                } catch (Exception e) {
                    Log.e(TAG, "Asc" + e.getMessage());
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "Asyc:" + e);
                return "Exception: " + e.getMessage();
            }
            return current;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d(TAG,"Process: "+context.getIntent().getStringExtra("process"));
            Log.d("onPostExecute", "Response " +context.getIntent().getStringExtra("process")+":"+ s);

            String jsonField = null;
            JSONObject rootjson , datajson = null;

            try {
                rootjson = new JSONObject(s);
                if (rootjson.has("data")) {
                    jsonField = "data";
                } else if (rootjson.has("error")) {
                    jsonField = "error";
                }
                if(jsonField != null)
                    datajson = rootjson.getJSONObject(jsonField);
            } catch (NullPointerException ex) {
                ex.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(jsonField == null || datajson ==null) {
                Log.i(TAG,"No Result Found");
                Toast.makeText(getApplicationContext(),"No result Found",Toast.LENGTH_LONG).show();
                mProgressDialog.hide();
            } else if (jsonField.equals("error")) {
                Toast.makeText(getApplicationContext(), "Parse Error, Reason " + datajson.optString("reason"), Toast.LENGTH_LONG).show();
                Log.e(TAG, "Error code:" + datajson.optString("code"));
                Log.e(TAG, "Error domain:" + datajson.optString("domain"));
                Log.e(TAG, "Error reason:" + datajson.optString("reason"));
                Log.e(TAG, "Error message:" + datajson.optString("message"));
                mProgressDialog.hide();
                finish();
            } else {
                Log.i("Response","Received");
                JSONObject info = datajson.optJSONObject("info");
                String name, father_name, dob;
                mProgressDialog.hide();
                switch (context.getIntent().getStringExtra("process")) {
                    case "pan":
                        Log.i(TAG,"Pan");
                        String pan;

                        name = info.optString("name");
                        father_name = info.optString("father_name");
                        pan = info.optString("pan_id");
                        JSONObject datejson = info.optJSONObject("date");
                        dob = getDate(datejson);

                        PanDetails p = new PanDetails(name, father_name, pan, dob);
                        panScannerListener.onResult(p);
                        panScannerListener = null;
                        finish();
                        break;
                    case "aadhar":
                        Log.i(TAG,"aadhar");
                        String aadhar, gender;
                        JSONArray addressArray;

                        name = getStringFromArray(info.optJSONArray("name"));
                        aadhar = info.optString("aadhaar_id");
                        gender = info.optString("gender");
                        addressArray = info.optJSONArray("address");
                        JSONObject dateAjson = info.optJSONObject("date_of_birth");
                        dob = getDate(dateAjson);

                        AadharDetails ad = new AadharDetails(name, gender, aadhar, dob,addressArray);
                        aadhaarScannerListener.onResult(ad);
                        aadhaarScannerListener = null;
                        context.finish();
                        break;
                    case "driving":
                        Log.e(TAG, "DrivingLicense");
                        String dlId, dlState, swdOf, bloodGroup,
                                dateOfExpiryT, dateOfExpiryNT,dateOfBirth,dateOfIssue;
                        JSONArray drinvingAddress;

                        name = getStringFromArray(info.optJSONArray("name"));
                        dlId = info.optString("dl_id");
                        dlState = info.optString("dl_state");
                        swdOf = getStringFromArray(info.optJSONArray("swd_of"));
                        bloodGroup = info.optString("bloof_group");
                        dateOfBirth = getDate(info.optJSONObject("date_of_birth"));
                        dateOfIssue = getDate(info.optJSONObject("date_of_issue"));
                        dateOfExpiryT = getDate(info.optJSONObject("date_of_expiry_t"));
                        dateOfExpiryNT = getDate(info.optJSONObject("date_of_expiry_nt"));
                        drinvingAddress = info.optJSONArray("address");

                        DrivingLicenseDetails drivingLicenseDetails = new DrivingLicenseDetails(name, dlId, dlState, swdOf, bloodGroup, dateOfExpiryT, dateOfExpiryNT, drinvingAddress, dateOfBirth, dateOfIssue);
                        drivingScannerListener.onResult(drivingLicenseDetails);
                        drivingScannerListener = null;
                        context.finish();
                        break;
                    case "cheque":
                        Log.e(TAG, "cheque");
                        String bank = null, branch = null, ifsc = null, city = null,
                                district = null, state = null, micr = null, accountNo, address = null;

                        if (info.has("bank"))
                            bank = info.optJSONObject("bank").optString("value");
                        if (info.has("address"))
                            address = info.optJSONObject("address").optString("value");
                        if (info.has("branch"))
                            branch = info.optJSONObject("branch").optString("value");
                        if (info.has("ifsc"))
                            ifsc = info.optJSONObject("ifsc").optString("value");
                        if (info.has("city"))
                            city = info.optJSONObject("city").optString("value");
                        if (info.has("district"))
                            district = info.optJSONObject("district").optString("value");
                        if (info.has("state"))
                            state = info.optJSONObject("state").optString("value");
                        if (info.has("micr"))
                            micr = info.optJSONObject("micr").optString("value");

                        accountNo = info.optJSONObject("account_no").optString("value");
                        ChequeDetails chequeDetails = new ChequeDetails(bank, address, branch, ifsc, city, district, state, micr, accountNo);
                        chequeScannerListener.onResult(chequeDetails);
                        chequeScannerListener = null;
                        context.finish();
                        break;
                    case "passport":
                        Log.e(TAG, "passport");
                        String surname, id, country, placeBirth, type,
                                placeIssue, dateOfExpiry,sex;

                        name = getStringFromArray(info.optJSONArray("name"));
                        id = info.optString("id");
                        surname = getStringFromArray(info.optJSONArray("surname"));
                        country = info.optString("country");
                        type = info.optString("type");
                        sex = info.optString("sex");
                        placeBirth = info.optString("place_birth");
                        placeIssue = info.optString("place_issue");
                        dateOfBirth = getDate(info.optJSONObject("date_of_birth"));
                        dateOfExpiry = getDate(info.optJSONObject("date_of_expiry"));
                        dateOfIssue = getDate(info.optJSONObject("date_of_issue"));

                        PassportDetails passportDetails = new PassportDetails(name, surname, id, country, sex, placeBirth, type, placeIssue, dateOfBirth,
                                dateOfIssue, dateOfExpiry);
                        passportScannerListener.onResult(passportDetails);
                        passportScannerListener = null;
                        context.finish();
                        break;
                    case "voterid":
                        Log.e(TAG, "voterid");
                        String fatherName , voterId;

                        name = getStringFromArray(info.optJSONArray("name"));
                        fatherName = getStringFromArray(info.optJSONArray("father_name"));
                        voterId = info.optString("voter_id");
                        sex = info.optString("sex");
                        dateOfBirth = getDate(info.optJSONObject("date_of_birth"));

                        VoterIdDetails voterIdDetails = new VoterIdDetails(name, fatherName, voterId, sex, dateOfBirth);
                        voterIdScannerListener.onResult(voterIdDetails);
                        voterIdScannerListener = null;
                        context.finish();
                        break;
                }
            }
        }
        String getDate(JSONObject dateObject) {
            String date = null;
            if (dateObject != null) {
                date = String.valueOf(dateObject.optInt("date")) + "/" +
                        String.valueOf(dateObject.optInt("month")) + "/" +
                        String.valueOf(dateObject.optInt("year"));
            }
            return date;
        }

        String getStringFromArray(JSONArray nameArray) {
            StringBuilder result = null;
            if (nameArray != null) {
                for (int count = 0; count < nameArray.length(); count++) {
                    if (count == 0) {
                        result = new StringBuilder(String.valueOf(nameArray.opt(0)));
                    } else {
                        result.append(" ").append(String.valueOf(nameArray.opt(count)));
                    }
                }
                return result.toString();
            }
            return null;
        }

    }
}
