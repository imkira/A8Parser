package com.autonom8.parser.a8;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.autonom8.parser.a8.interfaces.ImageScannerListener;
import com.autonom8.parser.a8.pojo.ChequeDetails;
import com.autonom8.parser.a8.pojo.DrivingLicenseDetails;
import com.autonom8.parser.a8.pojo.PassportDetails;
import com.autonom8.parser.a8.pojo.VoterIdDetails;
import com.autonom8.parser.imagescanner.ImageScannerActivity;
import com.autonom8.parser.a8.pojo.AadharDetails;
import com.autonom8.parser.a8.pojo.PanDetails;


public class A8Parser {
    private static  A8Parser a8;
    private Context mcontext;
    private String TAG = "A8Parser";
    public static String appKey="";
    private String panUrl ="https://api-parser.autonom8.com/pan";
    private String panVersion = "0.2";

    private String aadharUrl="https://api-parser.autonom8.com/aadhaar";
    private String aadharVersion = "0.3";

    private String drivingLicenseUrl="https://api-parser.autonom8.com/driving-license";
    private String drivingLicenseVersion = "0.1";

    private String chequeUrl = "https://api-parser.autonom8.com/cheque";
    private String chequeVersion = "0.3";

    private String passportUrl  = "https://api-parser.autonom8.com/passport";
    private String passportVersion = "0.3";

    private String voterIdUrl = "https://api-parser.autonom8.com/voter-id";
    private String voterIdVersion = "0.1";

    private A8Parser(){

    }

    public static A8Parser getInstances(){
        if(a8==null) {
            a8 = new A8Parser();
        }
        return a8;
    }

    public void initalize(Context context, String appKey) {
        mcontext = context;
        A8Parser.appKey =appKey;
    }

    public void getPanDetails(ImageScannerListener<PanDetails> imageScannerListener){
        Log.e("OnMethod","getPan");
        try {
            ImageScannerActivity.panScannerListener = imageScannerListener;
            Intent takePictureIntent = new Intent(this.mcontext,ImageScannerActivity.class);
            takePictureIntent.putExtra("process","pan");
            takePictureIntent.putExtra("version",panVersion);
            takePictureIntent.putExtra("url", panUrl);
            this.mcontext.startActivity(takePictureIntent);
        } catch (Exception e) {
            Log.e("Error","getPan: "+e);
        }
    }
    public void getAadharDetails(String side, ImageScannerListener<AadharDetails> imageScannerListener) {
        Log.e("OnMethod", "getAadhar");
        Log.e("Aadhar", "Side" + side);
        Log.e("Aadhar", "Url:" + aadharUrl);
        try {
            ImageScannerActivity.aadhaarScannerListener = imageScannerListener;
            Intent takePictureIntent = new Intent(this.mcontext, ImageScannerActivity.class);
            takePictureIntent.putExtra("process", "aadhar");
            takePictureIntent.putExtra("version", aadharVersion);
            takePictureIntent.putExtra("url", aadharUrl);
            takePictureIntent.putExtra("side", side);
            mcontext.startActivity(takePictureIntent);
        } catch (Exception var4) {
            Log.e("Error", "getAadhar: " + var4);
        }
    }

    public void getDrivingLicenseDetails(ImageScannerListener<DrivingLicenseDetails> imageScannerListener){
        Log.e("OnMethod", "getDrivingLicense");
        try{
            ImageScannerActivity.drivingScannerListener = imageScannerListener;
            Intent takePictureIntent = new Intent(mcontext, ImageScannerActivity.class);
            takePictureIntent.putExtra("process", "driving");
            takePictureIntent.putExtra("version", drivingLicenseVersion);
            takePictureIntent.putExtra("url", drivingLicenseUrl);
            mcontext.startActivity(takePictureIntent);
        } catch(Exception ex){
            Log.e(TAG,"Error:"+ex.getMessage());
        }
    }

    public void getChequeDetails(ImageScannerListener<ChequeDetails> imageScannerListener){
        try{
            ImageScannerActivity.chequeScannerListener = imageScannerListener;
            Intent takePictureIntent = new Intent(mcontext, ImageScannerActivity.class);
            takePictureIntent.putExtra("process", "cheque");
            takePictureIntent.putExtra("version", chequeVersion);
            takePictureIntent.putExtra("url", chequeUrl);
            mcontext.startActivity(takePictureIntent);
        } catch(Exception ex){
            Log.e(TAG,"Error:"+ex.getMessage());
        }
    }


    public void getPassportDetails(String side,ImageScannerListener<PassportDetails> imageScannerListener){
        try{
            ImageScannerActivity.passportScannerListener = imageScannerListener;
            Intent takePictureIntent = new Intent(mcontext, ImageScannerActivity.class);
            takePictureIntent.putExtra("process", "passport");
            takePictureIntent.putExtra("side", side);
            takePictureIntent.putExtra("version",passportVersion );
            takePictureIntent.putExtra("url", passportUrl);
            mcontext.startActivity(takePictureIntent);
        } catch(Exception ex){
            Log.e(TAG,"Error:"+ex.getMessage());
        }
    }

    public void getVoterIdDetails(ImageScannerListener<VoterIdDetails> imageScannerListener){
        try{
            ImageScannerActivity.voterIdScannerListener = imageScannerListener;
            Intent takePictureIntent = new Intent(mcontext, ImageScannerActivity.class);
            takePictureIntent.putExtra("process", "voterid");
            takePictureIntent.putExtra("version", voterIdVersion);
            takePictureIntent.putExtra("url", voterIdUrl);
            mcontext.startActivity(takePictureIntent);
        } catch(Exception ex){
            Log.e(TAG,"Error:"+ex.getMessage());
        }
    }

    public void setPanApi(String URL,String version){
        panVersion = version;
        panUrl =URL;
    }
    public void setAadharApi(String URL,String version){
        aadharVersion = version;
        aadharUrl =URL;
    }

    public void setDrivingLicenseApi(String drivingLicenseUrl,String version) {
        drivingLicenseVersion =version;
        this.drivingLicenseUrl = drivingLicenseUrl;
    }

    public void setChequeApi(String chequeUrl,String version) {
        chequeVersion =version;
        this.chequeUrl = chequeUrl;
    }

    public void setPassportApi(String passportUrl,String version) {
        passportVersion = version;
        this.passportUrl = passportUrl;
    }

    public void setVoterIdApi(String voterIdUrl,String version) {
        voterIdVersion = version;
        this.voterIdUrl = voterIdUrl;
    }
}
