package com.autonom8.parser.a8.pojo;


public class PanDetails {
    private String Name;
    private String FathersName;
    private String AccountNumber;
    private String Dob;

    public PanDetails() {
    }

    public PanDetails(String name, String fathersName, String accountNumber, String dob) {
        Name = name;
        FathersName = fathersName;
        AccountNumber = accountNumber;
        Dob = dob;
    }

    public String getName() {
        return Name;
    }

    public String getFathersName() {
        return FathersName;
    }

    public String getAccountNumber() {
        return AccountNumber;
    }
    public String getDob() {
        return Dob;
    }
}
