package com.autonom8.parser.a8.pojo;

import org.json.JSONArray;

public class DrivingLicenseDetails {
    String name = null,dlId = null, dlState = null, swdOf = null, bloodGroup = null,
            dateOfExpiryT = null, dateOfExpiryNT = null,dateOfBirth = null, dateOfIssue = null;
    JSONArray address;

    public DrivingLicenseDetails(String name,String dlId, String dlState, String swdOf, String bloodGroup, String dateOfExpiryT, String dateOfExpiryNT, JSONArray address, String dateOfBirth, String dateOfIssue) {
        this.name = name;
        this.dlId = dlId;
        this.dlState = dlState;
        this.swdOf = swdOf;
        this.bloodGroup = bloodGroup;
        this.dateOfExpiryT = dateOfExpiryT;
        this.dateOfExpiryNT = dateOfExpiryNT;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
        this.dateOfIssue = dateOfIssue;
    }

    public String getName() {
        return name;
    }

    public String getDlId() {
        return dlId;
    }

    public String getDlState() {
        return dlState;
    }

    public String getSwdOf() {
        return swdOf;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public String getDateOfExpiryT() {
        return dateOfExpiryT;
    }

    public String getDateOfExpiryNT() {
        return dateOfExpiryNT;
    }

    public JSONArray getAddress() {
        return address;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getDateOfIssue() {
        return dateOfIssue;
    }

    public DrivingLicenseDetails() {
    }

}
