package com.autonom8.parser.a8;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.autonom8.parser.R;

public class A8Designer {
    private String LeftText,RightText,MiddleText,CropLeftText,CropRightText,bg;
    private int LeftIcon,RightIcon,MiddleIcon,bgcolor,cropRightIcon,cropLeftIcon;
    public static Activity activity;
    private TextView leftTv,middleTv,rightTv,cropLeftTv,cropRightTv;
    private LinearLayout cropLl,processLl;

    public A8Designer(){
        bindViews();
        Log.e("A8Designer","value:"+activity.findViewById(R.id.bt_back_trim));
    }

    public A8Designer setCropRightText(String Text,int icon){
        this.CropRightText=Text;
        this.cropRightIcon=icon;
        return this;
    }
    public A8Designer setCropLeftText(String Text,int icon){
        this.CropLeftText=Text;
        this.cropLeftIcon=icon;
        return this;
    }public A8Designer setLeftText(String Text,int icon){
        this.LeftText=Text;
        this.LeftIcon=icon;
        return this;
    }
    public A8Designer setRightText(String Text,int icon){
        this.RightText=Text;
        this.RightIcon=icon;
        return this;
    }
    public A8Designer setMiddleText(String Text,int icon){
        this.MiddleText=Text;
        this.MiddleIcon=icon;
        return this;
    }
    public A8Designer setBackgroundColor(int color){
        bgcolor=color;
        return this;
    }
    public A8Designer setBackgroundColor(String hexCode){
        bg=hexCode;
        return this;
    }
    public void build(){
        //setText
        Log.e("Middle","Text"+MiddleText);
//        cropLeftTv.setText(CropLeftText!=null?CropLeftText:);
        if(CropLeftText!=null)
            cropLeftTv.setText(CropLeftText);
        if(CropRightText!=null)
            cropRightTv.setText(CropRightText);

        if(MiddleText!=null)
            middleTv.setText(MiddleText);
        if(LeftText!=null)
            leftTv.setText(LeftText);
        if(RightText!=null)
            rightTv.setText(RightText);

        //setBackGroundColor
        if(bgcolor!=0) {
            cropLl.setBackgroundColor(bgcolor);
            processLl.setBackgroundColor(bgcolor);
        }
        if(bg!=null){
            cropLl.setBackgroundColor(Color.parseColor(bg));
            processLl.setBackgroundColor(Color.parseColor(bg));
        }

        //setIcon
        if(cropRightIcon!=0)
            cropRightTv.setCompoundDrawablesWithIntrinsicBounds(null,activity.getApplicationContext().getResources().getDrawable(cropRightIcon),null,null);
        if(cropLeftIcon!=0)
            cropLeftTv.setCompoundDrawablesWithIntrinsicBounds(null,activity.getApplicationContext().getResources().getDrawable(cropLeftIcon),null,null);

        if(LeftIcon!=0)
            leftTv.setCompoundDrawablesWithIntrinsicBounds(null,activity.getApplicationContext().getResources().getDrawable(LeftIcon),null,null);
        if(MiddleIcon!=0)
            middleTv.setCompoundDrawablesWithIntrinsicBounds(null,activity.getApplicationContext().getResources().getDrawable(MiddleIcon),null,null);
        if(RightIcon!=0)
            rightTv.setCompoundDrawablesWithIntrinsicBounds(null,activity.getApplicationContext().getResources().getDrawable(RightIcon),null,null);
    }
    private void bindViews(){
        //Crop Panel
        cropLeftTv= activity.findViewById(R.id.bt_back_add);
        cropRightTv = activity.findViewById(R.id.bt_enhance);
        //Save Panel
        leftTv = activity.findViewById(R.id.bt_back_trim);
        rightTv = activity.findViewById(R.id.bt_rotate);
        middleTv = activity.findViewById(R.id.bt_save);

        cropLl = activity.findViewById(R.id.panel);
        processLl = activity.findViewById(R.id.bottomPanel);

    }
}
