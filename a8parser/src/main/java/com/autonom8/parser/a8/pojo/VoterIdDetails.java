package com.autonom8.parser.a8.pojo;

public class VoterIdDetails {
    String name = null,fatherName = null, voterId = null, sex = null, dateOfBirth =null;

    public VoterIdDetails() {
    }

    public VoterIdDetails(String name, String fatherName, String voterId, String sex, String dateOfBirth) {
        this.name = name;
        this.fatherName = fatherName;
        this.voterId = voterId;
        this.sex = sex;
        this.dateOfBirth = dateOfBirth;
    }

    public String getFatherName() {
        return fatherName;
    }

    public String getName() {
        return name;
    }

    public String getVoterId() {
        return voterId;
    }

    public String getSex() {
        return sex;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }
}
