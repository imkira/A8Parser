package com.autonom8.parser.a8.pojo;

public class ChequeDetails {
    String bank = null, address = null, branch = null, ifsc = null, city = null,
            district = null, state = null, micr = null, accountNo = null;

    public ChequeDetails() {

    }

    public ChequeDetails(String bank, String address, String branch, String ifsc, String city, String district, String state, String micr, String accountNo) {
        this.bank = bank;
        this.address = address;
        this.branch = branch;
        this.ifsc = ifsc;
        this.city = city;
        this.district = district;
        this.state = state;
        this.micr = micr;
        this.accountNo = accountNo;
    }

    public String getBank() {
        return bank;
    }

    public String getAddress() {
        return address;
    }

    public String getBranch() {
        return branch;
    }

    public String getIfsc() {
        return ifsc;
    }

    public String getCity() {
        return city;
    }

    public String getDistrict() {
        return district;
    }

    public String getState() {
        return state;
    }

    public String getMicr() {
        return micr;
    }

    public String getAccountNo() {
        return accountNo;
    }
}

