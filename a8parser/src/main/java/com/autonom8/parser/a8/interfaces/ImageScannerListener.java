package com.autonom8.parser.a8.interfaces;

import java.io.Serializable;

public interface ImageScannerListener<T> extends Serializable{
//    void onStart();
    void onResult(T result);
}
