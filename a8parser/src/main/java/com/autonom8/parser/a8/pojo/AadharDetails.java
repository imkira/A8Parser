package com.autonom8.parser.a8.pojo;

import org.json.JSONArray;

public class AadharDetails {
    private String name;
    //    private String father_name;
    private String gender;
    private String AadharNumber;
    private String dob;
    private JSONArray address;

    public AadharDetails() {
    }

    public AadharDetails(String name, String gender, String aadharNumber, String dob, JSONArray address) {
        this.name = name;
        this.gender = gender;
        AadharNumber = aadharNumber;
        this.dob = dob;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getAadharNumber() {
        return AadharNumber;
    }

    public String getDob() {
        return dob;
    }

    public JSONArray getAddress() {
        return address;
    }

    public void setAddress(JSONArray address) {
        this.address = address;
    }

}
