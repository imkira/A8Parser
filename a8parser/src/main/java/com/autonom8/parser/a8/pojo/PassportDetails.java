package com.autonom8.parser.a8.pojo;

public class PassportDetails {
    String name=null, surname = null, id = null, country = null,sex = null,placeBirth = null,type = null,
            placeIssue = null, dateOfBirth = null, dateOfIssue = null, dateOfExpiry = null;

    public PassportDetails() {
    }

    public PassportDetails(String name, String surname, String id, String country, String sex, String placeBirth, String type, String placeIssue, String dateOfBirth, String dateOfIssue, String dateOfExpiry) {
        this.name = name;
        this.surname = surname;
        this.id = id;
        this.country = country;
        this.sex = sex;
        this.placeBirth = placeBirth;
        this.type = type;
        this.placeIssue = placeIssue;
        this.dateOfBirth = dateOfBirth;
        this.dateOfIssue = dateOfIssue;
        this.dateOfExpiry = dateOfExpiry;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getId() {
        return id;
    }

    public String getCountry() {
        return country;
    }

    public String getSex() {
        return sex;
    }

    public String getPlaceBirth() {
        return placeBirth;
    }

    public String getType() {
        return type;
    }

    public String getPlaceIssue() {
        return placeIssue;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getDateOfIssue() {
        return dateOfIssue;
    }

    public String getDateOfExpiry() {
        return dateOfExpiry;
    }
}
